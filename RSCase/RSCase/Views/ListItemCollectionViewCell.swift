//
//  ListItemCollectionViewCell.swift
//  RSCase
//
//  Created by AppLogist on 16.04.2021.
//

import UIKit

final class ListItemCollectionViewCell: UICollectionViewCell {
    
    fileprivate lazy var playButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(systemName: "play.fill"), for: .normal)
        return btn
    }()
    
    fileprivate lazy var stackView : UIStackView = {
       let stackView = UIStackView()
        stackView.axis = .horizontal
        return stackView
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
       let label = UILabel()
       label.font = .boldSystemFont(ofSize: 17)
       label.textColor = .darkText
       return label
    }()
    
    
    fileprivate lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .secondarySystemBackground
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    // MARK: - Setup UI
    private func setupUI() {
        addStackView()
        addTitleLabel()
        addPlayButton()
        addLine()
    }
    
    func addStackView() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        contentView.setNeedsLayout()
    }
    
    func addTitleLabel() {
        stackView.addArrangedSubview(titleLabel)
    }
    
    func addPlayButton() {
        stackView.addArrangedSubview(playButton)
    }
    
    func addLine() {
        lineView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(lineView)
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        lineView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        lineView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        lineView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        contentView.setNeedsLayout()
    }
    
    func setup(with title: String?) {
        titleLabel.text = title
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
    }
}
