//
//  DocumentsManager.swift
//  RSCase
//
//  Created by AppLogist on 17.04.2021.
//

import Foundation

class DocumentsManager: NSObject {
    static func listFilesFromDocumentsFolder() -> [String] {
        let dirs = NSSearchPathForDirectoriesInDomains(.documentDirectory, .allDomainsMask, true)
        if dirs.isEmpty { return [""] }
        guard let dir = dirs.first,
              let fileList = try? FileManager.default.contentsOfDirectory(atPath: dir) else { return [""] }
        return fileList
    }
    
    static func hasFile(with name: String) -> Bool { listFilesFromDocumentsFolder().contains(name.fileFormat) }
    
    static func clearDocuments() {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask).first
        listFilesFromDocumentsFolder().forEach({
            debugPrint($0)
            if let filePath = documentsURL?.appendingPathComponent($0),
               FileManager.default.fileExists(atPath: filePath.path) {
                do {
                    try FileManager.default.removeItem(atPath: filePath.path)
                }
                catch {
                    print ("Error deleting file")
                }
            }
        })
        debugPrint("Records deleted")
    }
}
