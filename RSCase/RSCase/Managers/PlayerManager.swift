//
//  PlayerManager.swift
//  RSCase
//
//  Created by AppLogist on 18.04.2021.
//

import UIKit
import AVKit

class PlayerManager {
    
    private let target: UIViewController?
    
    init(_ target: UIViewController) {
        self.target = target
    }
    
    func playVideo(with name: String) {
        let fm = FileManager.default
        guard let docsURL = try? fm.url(for:.documentDirectory,
                                        in: .userDomainMask,
                                        appropriateFor: nil,
                                        create: false) else { return }
        let path = docsURL.appendingPathComponent(name).absoluteString
        playVideo(filePath: path)
    }
    
    private func playVideo(filePath:String){
        let playerItem = AVPlayerItem(url: URL(fileURLWithPath: filePath))
        let player = AVPlayer(playerItem: playerItem)
        let playerController = AVPlayerViewController()
        playerController.player = player
        player.play()
        target?.navigationController?.present(playerController, animated: true)
    }
}
