//
//  VideoManager.swift
//  RSCase
//
//  Created by AppLogist on 17.04.2021.
//

import UIKit
import AVFoundation

protocol VideoManagerDelegate: class {
    func didFinishRecording(_ outputFileURL: URL)
}

final class VideoManager: NSObject, AVCaptureFileOutputRecordingDelegate {
    
    private var viewController: UIViewController!
    
    private var captureSession = AVCaptureSession()
    private var sessionOutput = AVCaptureVideoDataOutput()
    private var movieOutput = AVCaptureMovieFileOutput()
    private var previewLayer = AVCaptureVideoPreviewLayer()
    private var mediaType : AVMediaType = .video
    
    private weak var delegate: VideoManagerDelegate?
    
    init(_ vc: UIViewController, delegate: VideoManagerDelegate? = nil) {
        self.viewController = vc
        self.delegate = delegate
    }
    
    
    private var hasPermission : Bool { AVCaptureDevice.authorizationStatus(for: mediaType) != .notDetermined }

    func getPermission() {
        if hasPermission { return }
        AVCaptureDevice.requestAccess(for: mediaType) { (response) in
            debugPrint(response)
        }
    }
    
    func fileOutput(_ output: AVCaptureFileOutput,
                    didFinishRecordingTo outputFileURL: URL,
                    from connections: [AVCaptureConnection], error: Error?) {
        if let error = error {
            debugPrint("FINISHED \(error.localizedDescription)")
            return
        }
        
        self.delegate?.didFinishRecording(outputFileURL)
    }
    
    func captureVideo(_ view: UIView) {
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera],
                                                       mediaType: mediaType,
                                                       position: .unspecified)
        for device in discoverySession.devices {
            guard device.position == AVCaptureDevice.Position.back else { return }
            guard let input = try? AVCaptureDeviceInput(device: device) else {
                debugPrint("Error")
                return
            }
            
            if !captureSession.canAddInput(input) { return }
                
            captureSession.addInput(input)
            sessionOutput.recommendedVideoSettings(forVideoCodecType: .jpeg, assetWriterOutputFileType: .mov)
            
            if captureSession.canAddOutput(sessionOutput){
                
                captureSession.addOutput(sessionOutput)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer.videoGravity = .resizeAspectFill
                previewLayer.connection?.videoOrientation = .portrait
                view.layer.addSublayer(previewLayer)
                
                previewLayer.position = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2)
                previewLayer.bounds = view.frame
            }
            captureSession.addOutput(movieOutput)
            captureSession.startRunning()
        }
    }
    
    
    func startRecording(with fileName: String) {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        guard let fileUrl = paths.first?.appendingPathComponent("\(fileName.fileNameWithExtension)") else { return }
        try? FileManager.default.removeItem(at: fileUrl)
        movieOutput.startRecording(to: fileUrl, recordingDelegate: self)
    }
    
    func stopRecording() {
        movieOutput.stopRecording()
        debugPrint(DocumentsManager.listFilesFromDocumentsFolder())
    }
    
}

