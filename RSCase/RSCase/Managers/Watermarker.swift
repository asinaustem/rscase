//
//  Watermarker.swift
//  RSCase
//
//  Created by AppLogist on 18.04.2021.
//

import AVKit

final class Watermarker {
    
    func watermarkVideo(with name: String, shot: RSShot, completion: @escaping ((String?) -> ())) {
        guard let outputPathURL = outputFile(name: .watermark + name) else { return }
        guard let inputURL = getPath(for: name) else { return }
        guard let asset = getAsset(with: inputURL) else { return }
        
        guard let assetTrack = asset.tracks(withMediaType: .video).first else { return }
        let layers = addLayersOverTheVideo(shot: shot, inputClip: assetTrack)
        let compositionAndInstruction = videoCompotisionAndInstruction(videoLayer: layers.videoLayer, parentLayer: layers.parentLayer)
        let mutableVideoComposition =  rotateToPortrait(videoComposition: compositionAndInstruction.0,
                                                        instruction: compositionAndInstruction.1,
                                                        inputClip: assetTrack)
        exportVideo(asset: asset,
                    videoComposition: mutableVideoComposition,
                    outputPath: outputPathURL,
                    completion: completion)
    }
    
    private func getPath(for name: String) -> URL? {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask).first
        guard let path = documentsURL?.appendingPathComponent(name) else {
            assertionFailure("Output path not found")
            return nil
        }
        return path
    }
    
    private func outputFile(name: String) -> URL? {
        //output file
        guard let outputPath = getPath(for: name) else { return  nil }
        if FileManager.default.fileExists(atPath: outputPath.path) {
            do {
                try FileManager.default.removeItem(atPath: outputPath.path)
            }
            catch {
                print ("Error deleting file")
            }
        }
        return outputPath
    }
    
    private func getAsset(with filePath: URL) -> AVAsset? { AVAsset(url: filePath) }
    
    private func textLayer(key: String, value: String, in y: CGFloat) -> CALayer  {
        let textLayer = CATextLayer()
        textLayer.string = [key, value].joined(separator: " : ")
        textLayer.foregroundColor = UIColor.white.cgColor
        textLayer.font = UIFont.systemFont(ofSize: 20)
        textLayer.alignmentMode = .left
        textLayer.frame = CGRect(x: 0, y: y, width: 300, height: 100)
        textLayer.displayIfNeeded()
        return textLayer
    }
    
    private func addTextLayers(shot: RSShot, parentLayer: CALayer) {
        if let point = shot.point {
            parentLayer.addSublayer(textLayer(key: "point", value: String(point.description), in: 0))
        }
        if let segment = shot.segment {
            parentLayer.addSublayer(textLayer(key: "segment", value: String(segment), in: 100))
        }
        if let inOut = shot.inOut {
            parentLayer.addSublayer(textLayer(key: "inOut", value: String(inOut.description), in: 200))
        }
        if let shotPosX = shot.shotPosX {
            parentLayer.addSublayer(textLayer(key: "shotPosX", value: String(shotPosX), in: 300))
        }
        if let shotPosY = shot.shotPosY {
            parentLayer.addSublayer(textLayer(key: "shotPosY", value: String(shotPosY), in: 400))
        }
    }
    
    private func addLayersOverTheVideo(shot: RSShot, inputClip: AVAssetTrack) -> (videoLayer: CALayer, parentLayer: CALayer) {
        let videoSize = inputClip.naturalSize
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        
        parentLayer.frame = CGRect(x: 0, y: 0, width: videoSize.height, height: videoSize.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: videoSize.height, height: videoSize.height)
        parentLayer.addSublayer(videoLayer)
        addTextLayers(shot: shot, parentLayer: parentLayer)
        return (videoLayer, parentLayer)
    }
    
    private func videoCompotisionAndInstruction(videoLayer: CALayer,
                                        parentLayer: CALayer) -> (AVMutableVideoComposition,
                                                                  AVMutableVideoCompositionInstruction) {
        // make video composition
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderScale = 1.0
        videoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        // adding watermark to the video
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer,
                                                                             in: parentLayer)
        videoComposition.renderSize = CGSize(width: videoLayer.frame.width,
                                             height: videoLayer.frame.height)

        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(start: .zero,
                                                duration: CMTimeMakeWithSeconds(60, preferredTimescale: 30))
        return (videoComposition,instruction)
    }
    
    private func rotateToPortrait(videoComposition: AVMutableVideoComposition,
                          instruction: AVMutableVideoCompositionInstruction,
                          inputClip: AVAssetTrack) -> AVMutableVideoComposition {
        //rotate to potrait
        let transformer = AVMutableVideoCompositionLayerInstruction(assetTrack: inputClip)
        let t1 = CGAffineTransform(translationX: inputClip.naturalSize.height,
                                   y: -(inputClip.naturalSize.width - inputClip.naturalSize.height) / 2)
        let t2: CGAffineTransform = t1.rotated(by: .pi/2)
        let finalTransform: CGAffineTransform = t2
        transformer.setTransform(finalTransform, at: .zero)
        instruction.layerInstructions = [transformer]
        videoComposition.instructions = [instruction]
        return videoComposition
    }
    
    private func exportVideo(asset: AVAsset,
                     videoComposition: AVMutableVideoComposition,
                     outputPath: URL,
                     completion: @escaping ((String?) -> ())) {
        let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality)
        exporter?.outputFileType = .mov
        exporter?.outputURL = outputPath
        exporter?.videoComposition = videoComposition
        exporter?.exportAsynchronously {
            if exporter?.status == .completed {
                debugPrint("Export complete")
                DispatchQueue.main.async(execute: {
                    UISaveVideoAtPathToSavedPhotosAlbum(outputPath.absoluteString, nil, nil, nil)
                    completion(outputPath.absoluteString)
                })
                return
            } else if exporter?.status == .failed {
                debugPrint("Export failed - \(String(describing: exporter?.error))")
            }
            completion(nil)
            return
        }
    }
}
