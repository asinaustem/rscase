//
//  Data    Manager.swift
//  RSCase
//
//  Created by AppLogist on 16.04.2021.
//

import UIKit

struct DataManager {
    @UserDefault(key: "ud-data", defaultValue: [])
    static var data: [RSDatum]
}
