//
//  RSShot.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import Foundation

struct RSShot : Codable {

    let id : String?
    let inOut : Bool?
    let point : Int?
    let segment : Int?
    let shotPosX : Float?
    let shotPosY : Float?
    
}
