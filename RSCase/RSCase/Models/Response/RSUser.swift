//
//  RSUser.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import Foundation

struct RSUser : Codable {

    let name : String?
    let surname : String?
    
}

extension RSUser {
    var nameAndSurname: String {
        [name,surname].compactMap({ $0 }).joined(separator: " ")
    }
}
