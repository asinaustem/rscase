//
//  RSBaseResponse.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import Foundation

struct RSBaseResponse<T: Codable>: Codable {
    public let success: Bool?
    public let data: T?
}
