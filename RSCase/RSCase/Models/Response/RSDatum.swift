//
//  RSDatum.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import Foundation

struct RSDatum : Codable {

    let shots : [RSShot]?
    let user : RSUser?
    
}
