//
//  String+Ext.swift
//  RSCase
//
//  Created by AppLogist on 18.04.2021.
//

import Foundation

extension String {
    var fileFormat : String {
        self.replacingOccurrences(of: " ", with: "_")
    }
    
    var fileNameWithExtension: String {
        self.fileFormat.appending(".mov")
    }
    
    static let watermark = "Watermarked_"
}
