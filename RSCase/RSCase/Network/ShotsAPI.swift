//
//  ShotsAPI.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import Foundation

enum ShotsAPI {
    case shots
}

extension ShotsAPI: Endpoint {
    
    var base: String { "http://ec2-18-188-69-79.us-east-2.compute.amazonaws.com:3000" }
    
    var path: String { "/shots" }
}
