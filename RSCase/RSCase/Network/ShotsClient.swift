//
//  ShotsClient.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import Foundation

final class ShotsClient: APIClient {

    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    //in the signature of the function in the success case we define the Class type thats is the generic one in the API
    func getShots(from api: ShotsAPI, completion: @escaping (Result<RSBaseResponse<[RSDatum]>?, APIError>) -> Void) {
        fetch(with: api.request ,
              decode: { json -> RSBaseResponse<[RSDatum]>? in
                guard let movieFeedResult = json as? RSBaseResponse<[RSDatum]> else { return  nil }
                return movieFeedResult
              },
              completion: completion)
    }
}
