//
//  APIClient.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import Foundation

typealias JSONTaskCompletionHandler = (Decodable?, APIError?) -> Void

protocol APIClient {
    var session: URLSession { get }
    func fetch<T: Decodable>(with request: URLRequest,
                             decode: @escaping (Decodable) -> T?,
                             completion: @escaping (Result<T, APIError>) -> Void)
}

extension APIClient {

    private func decodingTask<T: Decodable>(with request: URLRequest,
                                            decodingType: T.Type,
                                            completionHandler completion: @escaping JSONTaskCompletionHandler) -> URLSessionDataTask {
       
        let task = session.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, .requestFailed)
                return
            }
            if httpResponse.statusCode != 200 {
                completion(nil, .responseUnsuccessful)
                return
            }
            guard let data = data else {
                completion(nil, .invalidData)
                return
            }
            
            guard let genericModel = try? JSONDecoder().decode(decodingType, from: data) else {
                completion(nil, .jsonConversionFailure)
                return
            }
            completion(genericModel, nil)
        }
        return task
    }
    
    func fetch<T: Decodable>(with request: URLRequest,
                             decode: @escaping (Decodable) -> T?,
                             completion: @escaping (Result<T, APIError>) -> Void) {
        
        let task = decodingTask(with: request, decodingType: T.self) { (json , error) in
            
            //MARK: change to main queue
            DispatchQueue.main.async {
                guard let json = json else {
                    guard let error = error else {
                        completion(.failure(.invalidData))
                        return
                    }
                    completion(.failure(error))
                    return
                }
                guard let value = decode(json) else {
                    completion(.failure(.jsonParsingFailure))
                    return
                }
                completion(.success(value))
            }
        }
        task.resume()
    }
}
