//
//  AppDelegate.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:  [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupGlobalNavigationConfig()
        setAndNavigateToInitial(vc: ListViewController())
        
        return true
    }
    
    private func setAndNavigateToInitial(vc: UIViewController) {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: vc)
        window?.makeKeyAndVisible()
    }
    
    private func setupGlobalNavigationConfig() {
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.black,
                                                            .font: UIFont.boldSystemFont(ofSize: 20)]
    }
}

