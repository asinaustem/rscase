//
//  CaptureVideoViewController.swift
//  RSCase
//
//  Created by AppLogist on 17.04.2021.
//

import UIKit

class CaptureVideoViewController: UIViewController {
    
    private lazy var videoManager = VideoManager(self, delegate: self)
    var fileName: String = ""
    var shot : RSShot?
    
    private lazy var toggleButton: UIButton = {
        let button = UIButton()
        button.setTitle("Record", for: .normal)
        button.setTitle("Stop", for: .selected)
        button.setImage(UIImage(systemName: "record.circle")?.applyingSymbolConfiguration(.init(scale: .large)),
                        for: .normal)
        button.setImage(UIImage(systemName: "stop.circle")?.applyingSymbolConfiguration(.init(scale: .large)),
                        for: .selected)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(toggleButtonTapped(_:)), for: .touchUpInside)
        button.tintColor = .systemRed
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        videoManager.captureVideo(view)
        view.bringSubviewToFront(toggleButton)
    }
    
    // MARK: - Setup UI
    private func setupUI() {
        view.backgroundColor = .systemBackground
        addToggleButton()
    }
    
    private func addToggleButton() {
        toggleButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(toggleButton)
        toggleButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        toggleButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        toggleButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        toggleButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    @objc private func toggleButtonTapped(_ sender: UIButton) {
        toggleButton.isSelected ? videoManager.stopRecording() : videoManager.startRecording(with: fileName)
        toggleButton.isSelected.toggle()
    }
    
    func stopRecordAndDismiss() {
        videoManager.stopRecording()
    }
}


extension CaptureVideoViewController: VideoManagerDelegate {
    func didFinishRecording(_ outputFileURL: URL) {
        if !DocumentsManager.hasFile(with: fileName.fileNameWithExtension) { return }
        guard let shot = self.shot else { return }
        Watermarker().watermarkVideo(with: self.fileName.fileNameWithExtension, shot: shot) { [weak self] (outputPath) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                debugPrint("Watermarked File path:", outputPath ?? "")
                self.dismiss(animated: true)
            }
        }
    }
}
