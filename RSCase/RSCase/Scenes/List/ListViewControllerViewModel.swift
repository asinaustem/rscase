//
//  ListViewControllerViewModel.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import Foundation

final class ListViewControllerViewModel {
    
    private let client = ShotsClient()
    
    func getShots(completion: @escaping ((_ errorMessage: String?) -> ())) {
        client.getShots(from: .shots) { result in
            switch result {
            case .failure(let error):
                completion(error.localizedDescription)
            case .success(let response):
                DataManager.data = response?.data ?? []
                completion(nil)
            }
        }
    }
}
