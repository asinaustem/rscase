//
//  ListViewController.swift
//  RSCase
//
//  Created by AppLogist on 15.04.2021.
//

import UIKit
import AVFoundation

final class ListViewController: UIViewController {
    
    private let viewModel = ListViewControllerViewModel()
    private var collectionView : UICollectionView?
    private lazy var videoManager = VideoManager(self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getShots()
        videoManager.getPermission()
    }
    
    // MARK: - Services
    fileprivate func getShots() {
        viewModel.getShots { [weak self] errorMessage in
            guard let self = self else { return }
            if let errorMessage = errorMessage {
                debugPrint(errorMessage)
            }
            self.collectionView?.reloadData()
        }
    }
    
    // MARK: - Setup UI
    private func setupUI() {
        view.backgroundColor = .white
        setupNavigationBar()
        setupCollectionView()
    }
    
    private func setupNavigationBar() {
        title = "Shots"
        let clearButton = UIBarButtonItem(barButtonSystemItem: .trash,
                                          target: self,
                                          action: #selector(clearAllRecordsTapped))
        navigationItem.setRightBarButton(clearButton, animated: true)
    }
    
    @objc private func clearAllRecordsTapped() {
        DocumentsManager.clearDocuments()
    }
    
    private func setupCollectionView() {
        let view = UIView()
        view.backgroundColor = .white
        
        let flowLayout = UICollectionViewFlowLayout()
        // Now setup the flowLayout required for drawing the cells
        let space = 5.0 as CGFloat
        // Set left and right margins
        flowLayout.minimumInteritemSpacing = space
        // Set top and bottom margins
        flowLayout.minimumLineSpacing = space
        
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: flowLayout)
        collectionView?.register(ListItemCollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
        collectionView?.backgroundColor = UIColor.white
        
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        view.addSubview(collectionView ?? UICollectionView())
        
        self.view = view
    }
}

extension ListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        DataManager.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell",
                                                              for: indexPath) as? ListItemCollectionViewCell else {
            return UICollectionViewCell()
        }
        let item = DataManager.data[indexPath.row]
        myCell.setup(with: item.user?.nameAndSurname)
        return myCell
    }
}
extension ListViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = DataManager.data[indexPath.row]
        let name = (item.user?.nameAndSurname ?? "")
        if DocumentsManager.hasFile(with: .watermark + name.fileNameWithExtension) {
            PlayerManager(self).playVideo(with: .watermark + name.fileNameWithExtension)
            return
        }
        let captureVC = CaptureVideoViewController()
        captureVC.fileName = name
        captureVC.shot = item.shots?.first
        present(captureVC, animated: true)
    }
}
