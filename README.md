# RSCase
Code Case Challenge for **Rapsodo** (after the interview on 2 April 2021)

### Requirements
----------------------
-   Environment: **Xcode 10+**
-   Deployment Target: **iOS 13+**


### Description
-------------------
In this project, I wanted to show my approach rather than developing an user interface. 
* I did not use any 3rd party library and UI interface tools (xib, storyboard etc..). These were my preference.
* I used MVVM structure in this project and I extended these with Managers.
* I stored API data in UserDefaults. 
